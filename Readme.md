Dotvim
======

A collection of my VIM configuration and plugins.

Installation
------------

### Linux

1. Run homesick clone akrejczinger/dotvim
2. Run homesick symlink dotvim
3. Run plugin install from VIM:

    ```
    :PlugInstall
    ```
