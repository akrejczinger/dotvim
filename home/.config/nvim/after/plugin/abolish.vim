" Exit if :Abolish isn't available.
if !exists(':Abolish')
    finish
endif

Abolish balues values
Abolish bim vim
Abolish li{bv,bb,vb,vv}irt libvirt
Abolish esle else
Abolish wiht with
Abolish paht path
