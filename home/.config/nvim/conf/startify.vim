let g:ascii = [
    \ '   _  __             _',
    \ '  / |/ /__ ___ _  __(_)_ _',
    \ " /    / -_) _ \\ |/ / /  \' \\",
    \ '/_/|_/\__/\___/___/_/_/_/_/',
    \ ]
let g:startify_custom_header = g:ascii + startify#fortune#boxed()

let g:startify_lists = [
      \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
      \ { 'type': 'files',     'header': ['   MRU']            },
      \ ]
      " \ { 'type': 'sessions',  'header': ['   Sessions']       },
      " \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      " \ { 'type': 'commands',  'header': ['   Commands']       },
