" netrw config

" NOTE: this mapping replaced by ranger
" nnoremap - :Explore <CR>
let g:netrw_silent=1 " disable netrw command prompt 'ENTER' messages

augroup netrw_nerdlike_bindings
    autocmd!
    autocmd filetype netrw call Set_netrw_nerdlike_bindings()
augroup END
function! Set_netrw_nerdlike_bindings()
    nmap <buffer> j <Plug>VinegarUp
    nmap <buffer> l <CR>
    nnoremap <silent> <C-j> :TmuxNavigateLeft<cr>
    nnoremap <silent> <C-k> :TmuxNavigateDown<cr>
    nnoremap <silent> <C-h> :TmuxNavigateUp<cr>
    nnoremap <silent> <C-l> :TmuxNavigateRight<cr>
    nnoremap <silent> <C-z> :TmuxNavigatePrevious<cr>
endfunction
