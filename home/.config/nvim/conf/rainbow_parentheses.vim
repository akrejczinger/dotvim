let g:rbpt_colorpairs = [
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ]
    " \ ['brown',       'RoyalBlue3'],
    " \ ['brown',       'firebrick3'],

autocmd FileType clojure RainbowParenthesesActivate
" ()
autocmd FileType clojure RainbowParenthesesLoadRound
" []
autocmd FileType clojure RainbowParenthesesLoadSquare
" {}
autocmd FileType clojure RainbowParenthesesLoadBraces
" <>
autocmd FileType clojure RainbowParenthesesLoadChevrons
