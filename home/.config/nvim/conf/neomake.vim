" let g:neomake_python_enabled_makers = ['pyflakes', 'pylint']
let g:neomake_python_enabled_makers = ['flake8']
let g:neomake_perl_enabled_makers = ['perlcritic']
let g:neomake_javascript_enabled_makers = ['eslint']

call neomake#configure#automake('w')

" Replace ugly black highlight of errors
highlight! link NeomakeError SpellBad
