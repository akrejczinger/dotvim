" Mappings
" ========

noremap ; :

" Search for visually selected text
vnoremap // y/<C-r>"<CR>

" Backspace turns search highlight off
nnoremap <bs> :noh <cr>

" Colemak remappings:
noremap j h
noremap h k
noremap k j

noremap gj :wincmd h <cr>
noremap gl :wincmd l <cr>
noremap gh :wincmd k <cr>
noremap gk :wincmd j <cr>
noremap gw :wincmd w <cr>

" Useful window commands:
nnoremap <Leader>- :wincmd _ <cr>
nnoremap <Leader>= :wincmd = <cr>
nnoremap <Leader>' :wincmd <bar> <cr>

" Buffer jumping:
nnoremap <Leader>, :bprevious <cr>
nnoremap <Leader>. :bnext <cr>
nnoremap <Leader>x :Bdelete <cr>

nnoremap <Leader>r :redraw! <cr>

" Copy/paste
nnoremap cp "+y
nnoremap cP "+yy
nnoremap cv "+p
nnoremap cV "+P
" yank to end of line
nnoremap Y y$

" map space to be the same as \ leader key
map <Space> \

" map space+m to Magit buffer
map <Leader>m :Magit<cr>

" map Control+T to tagbar
map <Leader>t :TagbarOpenAutoClose<cr>
map <C-t> :TagbarToggle<cr>

" map to normal mode
inoremap <C-Space> <Esc>
tnoremap <C-Space> <C-\><C-n>

" Add undo breaks on newline
inoremap <cr> <C-g>u<cr>

" open a perltidy diff with a temporary file
function! Perltidy(...)
    let temp_file=tempname()
    silent exe '!perltidy % -o' temp_file '-q' a:1
    exe 'diffsplit' temp_file
    set nomodifiable
    redraw!
endfunction
command! -nargs=* Perltidy call Perltidy(<q-args>)

" Strip trailing whitespace
function! StripWs()
    silent execute '%s/\s\+$//'
endfunction
command! StripWs call StripWs()

" Edit awesomewm config
command! Naw execute 'e ~/.config/awesome/rc.lua'

" Edit init.vim
command! Nrc execute 'e ~/.config/nvim/init.vim'

" Edit plugins.vim
command! Npl execute 'e ~/.config/nvim/conf/plugins.vim'

" Formatting lists/dictionaries
function! Format() range
    " FIXME: this only works with visual line selections
    " save visual selection end
    let [lnum2, col2] = getpos("'>")[1:2]

    " start from cursor, return number of pattern matched, no wrapping
    let flags = 'pW'

    " Search for brackets start with optional trailing whitespace, comma, or
    " brackets end
    let bracket_start = '\([[({]\)'
    let bracket_end = '\([\])}]\)'
    let comma = '\(,\)'

    let found = 99
    while found >= 2
        let found = search(
            \bracket_start . '\|' . comma . '\|' . bracket_end,
            \flags,
            \lnum2)
        " TODO: why does it start at 2?
        if found == 2 " start
            execute "normal! a\<cr>    \<esc>"
            " we added a line, increase line limit
            let lnum2 = lnum2 + 1
        elseif found == 3 " comma
            :execute "normal! a\<cr>\<esc>"
            " we added a line, increase line limit
            let lnum2 = lnum2 + 1
        elseif found == 4 " end
            :execute "normal! i\<cr>\<bs>\<esc>l"
            " we added a line, increase line limit
            let lnum2 = lnum2 + 1
        endif
    endwhile
endfunction
command! -range Format call Format()

" Search for todo and similar comments in code
function! Todo()
    execute 'Ag "\b(TODO|BUG|FIXME|OPTIMIZE)\b"'
endfunction
command! TODO call Todo()
