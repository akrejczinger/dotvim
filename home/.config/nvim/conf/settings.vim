" Settings
" ========

" mouse
set ttyfast " faster mouse in tty
set mouse=a " mouse works in all modes

" bells
set vb " visual bell
set noeb " no audio bell

" diff
set diffopt+=vertical " vsplit by default

" hidden buffers
" set hidden " FIXME: neoterm creates empty buffers when this is set, workaround: see autowrite

" autowrite + undo
set autowriteall  " autosaves a file whenever switching buffers, hopefully hidden is not needed
set nohidden
set undolevels=10000
set undofile

" do not create swap files
set nobackup
set nowritebackup
set noswapfile

" searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" line editing options
set showcmd
set timeoutlen=500
set listchars=tab:»\ ,trail:·,extends:>,precedes:<
set list
set number " line numbering
set cursorline " highlight current line
set whichwrap+=b,s,h,l,<,>,[,]
set backspace=indent,eol,start
set scrolloff=5 " lines below/after cursor are always visible
set history=10000

" clipboard synced with other programs
set clipboard^=unnamed,unnamedplus

" colorcolumn is always same as textwidth (which is usually set by editorconfig)
let colorcolumn=+0

" tabs to spaces unless editorconfig overrides it
set tabstop=4
set shiftwidth=4
set expandtab

" show last line even if it doesn't fit fully on screen
set display+=lastline

" disable mode text in command area (still shown in airline)
set noshowmode

" delete comment character when joining commented lines
silent! set formatoptions+=j

" modeline
set modeline

" ignore certain files when editing
set wildignore=*.swp,*.bak,*.pyc,*.class

" case-insensitive filename completion
set wildignorecase

" better filename completion in command mode
set path+=**
set wildmenu

color slate
silent! color jay
if has('gui_running')
    " set guifont=DejaVu
endif
if has('win32') || has('win64')
    set guifont=Lucida_Console:h11:cANSI:qDRAFT
    set lines=47 columns=84
endif

" Fix indentation issues
filetype indent off
set autoindent
" Make indenting with = indent at parentheses instead of indent levels
set cinoptions+=(0

" Fix highlighting of matching parenthesis
hi MatchParen ctermbg=none ctermfg=yellow

" Shorter textwidth for git commits
autocmd FileType magit set textwidth=72

" Set markdown filetype on .md files
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

autocmd TermOpen * setlocal nonumber
" Always enter insert mode when entering terminal window
autocmd BufWinEnter,BufEnter term://.* startinsert
" I hate autocmd. This works as intended for terminals,
" except ranger which exits insert mode immediately.
" autocmd BufWinLeave,BufLeave term://.*!(ranger) stopinsert

function! LoadSyntaxCheckers()
    " checks if pep8 is installed
    silent exec "!flake8 -h"
    redr!
    if v:shell_error
        throw "Failed to run flake8 checker."
    endif
endfunction

" Auto-delete lingering netrw buffers
autocmd FileType netrw setl bufhidden=delete

" Auto-expand all folds in reStructuredText files
autocmd FileType rst exe "normal zR"

" Settings for Go language
autocmd FileType go setl smartindent
autocmd FileType go let b:AutoPairs = {'(':')', '[':']', '{':'}',"'":"'",'"':'"', '`':'`'}

" Enable project-specific .vimrc configs
set exrc
set secure
