let g:UltiSnipsEditSplit = "horizontal"
let g:UltiSnipsEnableSnipMate = 0

" Keybindings
let g:UltiSnipsExpandTrigger = "<c-e>"
let g:UltiSnipsJumpForwardTrigger = "<c-n>"
let g:UltiSnipsJumpBackwardTrigger = "<c-p>"
let g:UltiSnipsListSnippets = "<Leader>s"

" Python style settings
let g:ultisnips_python_style = "sphinx"
let g:ultisnips_python_quoting_style = "single"

let g:UltiSnipsSnippetDirectories = [
    \ $XDG_CONFIG_HOME ."/nvim/mysnips",
    \ $XDG_CONFIG_HOME ."/nvim/plugged/vim-snippets/UltiSnips",
\ ]

let g:UltiSnipsSnippetsDir = $XDG_CONFIG_HOME."/nvim/mysnips"
