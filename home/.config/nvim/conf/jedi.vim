let g:jedi#completions_enabled = 0
let g:jedi#auto_initialisation = 0
let g:jedi#auto_vim_configuration = 0
let g:jedi#show_call_signatures = 0

" Do not autocomplete when typing a dot
let g:jedi#popup_on_dot = 0

" Disable annoying auto insertion of 'import'
let g:jedi#smart_auto_mappings = 0

let g:jedi#force_py_version = 3
