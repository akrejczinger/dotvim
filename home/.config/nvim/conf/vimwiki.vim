let vimwiki_path=$HOME.'/vimwiki/'
let vimwiki_html_path=$HOME.'/vimwiki_html/'
" Fix issue: vimwiki bindings used for markdown
let g:vimwiki_global_ext = 0
let g:vimwiki_list = [{
    \ 'path': vimwiki_path,
    \ 'path_html': vimwiki_html_path,
    \ 'syntax': 'default',
    \ 'ext': '.wiki',
    \ 'template_path': vimwiki_html_path.'assets/',
    \ 'template_default': 'default',
    \ 'auto_export': 0 }]
