" Toggle terminal with alt+t
nnoremap <M-t> :botright Ttoggle<cr>
inoremap <M-t> <Esc>:botright Ttoggle<cr>
tnoremap <M-t> <C-\><C-n>:botright Ttoggle<cr>
