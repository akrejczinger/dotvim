let g:test#strategy = "neoterm"

" python
let g:test#python#runner = "pytest"

" nodejs
let g:test#javascript#runner = "mocha"
