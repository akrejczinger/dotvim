" Do not jump to another line when typing closing brackets
let g:AutoPairsMultilineClose = 0

" Disable AutoPairs, settings will re-enable later for go only (autocmd)
let g:AutoPairs = {}
