set noshowmode " mode would write over echodoc line, but it's shown in bufferline anyways
set completeopt-=preview " disable preview window popup for the function signature
let g:echodoc#enable_at_startup=1
