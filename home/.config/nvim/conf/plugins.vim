set nocompatible
filetype plugin indent on " might be required by some plugins
syn on

if has('win32') || has('win64')
  set runtimepath=$HOME/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,$HOME/.vim/after
endif

call plug#begin('~/.config/nvim/plugged')

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'Chun-Yang/vim-action-ag' " ag from normal mode, e.g. gagiw (inner word)
Plug 'LucHermitte/lh-vim-lib' " useful vimscript functions
Plug 'MarcWeber/vim-addon-local-vimrc'
Plug 'Shougo/echodoc.vim' " print function arguments in command area
Plug 'Shougo/neco-vim' " vimscript autocomplete
Plug 'SirVer/ultisnips' " Snippet plugin.
Plug 'aklt/plantuml-syntax' " PlantUML syntax highlighting and ftdetect
Plug 'alvan/vim-closetag'  " Auto-close HTML tags
Plug 'andymass/vim-matchup' " extends % to work with other tags
Plug 'benmills/vimux' " send command to tmux shell window
Plug 'blerins/flattown' " color scheme, unused for now
Plug 'christoomey/vim-tmux-navigator' " unified movement in vim and tmux
Plug 'dhruvasagar/vim-table-mode'
Plug 'dzeban/vim-log-syntax' " syntax highlighting for syslogs
Plug 'editorconfig/editorconfig-vim' " editor-agnostic style config
Plug 'ekalinin/Dockerfile.vim' " Dockerfile syntax and snippets
Plug 'elorest/vim-slang'
Plug 'fatih/vim-go'
" Plug 'francoiscabrol/ranger.vim' " Ranger file manager in vim
Plug 'ptzz/lf.vim' " lf file manager integration
Plug 'honza/vim-snippets' " Snippet library. Requires ultisnips.
Plug 'janko-m/vim-test'  " run unittests in various languages
Plug 'jiangmiao/auto-pairs' " auto insert parentheses and quotes pairs
Plug 'josuegaleas/jay' " alternate color scheme
Plug 'jreybert/vimagit' " easier git workflow from within vim
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim' " fuzzy finder for files, help, buffers...everything
Plug 'kassio/neoterm' " better neovim terminal handling
Plug 'kkoomen/vim-doge' " documentation generator (auto-sphinx for functions)
Plug 'lambdalisue/suda.vim' " SudoEdit.vim alternative for neovim
Plug 'ludovicchabant/vim-gutentags' " tag generation
Plug 'majutsushi/tagbar' " class/function outline browser
Plug 'mattn/calendar-vim'
Plug 'mbbill/undotree' " undo branches visualisation
Plug 'mhinz/vim-signify' " show changes in any VCS
Plug 'michaeljsmith/vim-indent-object' " 'i' as in indent
Plug 'moll/vim-bbye' " close buffers without closing window
Plug 'neoclide/coc.nvim', {'branch': 'release'} " autocomplete plugin, vscode server
Plug 'neomake/neomake' " async code checkers
Plug 'notpratheek/vim-luna' " color scheme for python
" Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'} " python semantic syntax highlight + renaming
Plug 'posva/vim-vue'  " VueJS syntax highlighting
Plug 'rbgrouleff/bclose.vim' " close buffer without closing window. Dependency of ranger.vim
Plug 'rhysd/vim-crystal'  " Crystal filetype, syntax highlight etc
Plug 'robbles/logstash.vim'  " Logstash highlighting
Plug 'elixir-editors/vim-elixir' " elixir plugin
Plug 'slashmili/alchemist.vim' " elixir alchemist plugin
Plug 'tommcdo/vim-lion' " alignment motions. gl=spaces left, gL=right.
Plug 'tomtom/tcomment_vim'  " embedded filetype supported commenting plugin
Plug 'tpope/vim-abolish' " fixing typos automatically.
Plug 'tpope/vim-dispatch' " asynchronously run plugins in background
Plug 'tpope/vim-endwise' " automatically closes if, do, etc. blocks with 'end'
Plug 'tpope/vim-fugitive' " git integration
Plug 'tpope/vim-projectionist' " project config files
Plug 'tpope/vim-repeat' " can repeat plugin actions with .
Plug 'tpope/vim-surround' " change/del surrounding quotes/tags/parens
Plug 'tpope/vim-unimpaired' " paired mappings prefixed with [ and ]
Plug 'tyru/open-browser.vim' " Opens URLs with the default browser
Plug 'itchyny/lightline.vim' " even more lightweight airline alternative
Plug 'vim-scripts/AnsiEsc.vim' " ANSI escape colors for piping from shell
Plug 'vimwiki/vimwiki'
Plug 'weirongxu/plantuml-previewer.vim' " depends on plantuml-syntax and open-browser

" Filetype-specific plugins:
Plug 'Shirk/vim-gas', {'for': 'asm'}
Plug 'digitaltoad/vim-pug', {'for': 'pug'}
Plug 'guns/vim-clojure-static', {'for': 'clojure'}
Plug 'guns/vim-clojure-highlight', {'for': 'clojure'}
Plug 'guns/vim-sexp', {'for': 'clojure'}
Plug 'tpope/vim-sexp-mappings-for-regular-people', {'for': 'clojure'}
Plug 'tpope/vim-leiningen', {'for': 'clojure'}
Plug 'tpope/vim-fireplace', {'for': 'clojure'}
Plug 'tpope/vim-salve', {'for': 'clojure'}
Plug 'kien/rainbow_parentheses.vim', {'for': 'clojure'} " why is this not working?
Plug 'rust-lang/rust.vim', {'for': 'rust'}

" augroup plug_xtype
"   autocmd FileType * if expand('<amatch>') != 'vue'
"   \| call plug#load('sheerun/vim-polyglot')
"   \| execute 'autocmd! plug_xtype' | endif
" augroup END

call plug#end()
