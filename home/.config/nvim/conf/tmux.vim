let g:tmux_navigator_no_mappings = 1

nnoremap <silent> <C-j> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-k> :TmuxNavigateDown<cr>
nnoremap <silent> <C-h> :TmuxNavigateUp<cr>
nnoremap <silent> <C-l> :TmuxNavigateRight<cr>

tnoremap <silent> <C-j> <C-\><C-n>:TmuxNavigateLeft<cr>
tnoremap <silent> <C-k> <C-\><C-n>:TmuxNavigateDown<cr>
tnoremap <silent> <C-h> <C-\><C-n>:TmuxNavigateUp<cr>
tnoremap <silent> <C-l> <C-\><C-n>:TmuxNavigateRight<cr>

" Always enter insert mode when entering terminal window
" autocmd BufWinEnter,WinEnter term://* startinsert
