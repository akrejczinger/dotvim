set nocompatible              " be iMproved, required
filetype off                  " required

" this must be first, otherwise things break:
source $XDG_CONFIG_HOME/nvim/conf/plugins.vim

source $XDG_CONFIG_HOME/nvim/conf/lightline.vim
source $XDG_CONFIG_HOME/nvim/conf/alchemist.vim
source $XDG_CONFIG_HOME/nvim/conf/auto-pairs.vim
source $XDG_CONFIG_HOME/nvim/conf/closetag.vim
source $XDG_CONFIG_HOME/nvim/conf/coc.vim
source $XDG_CONFIG_HOME/nvim/conf/crystal.vim
source $XDG_CONFIG_HOME/nvim/conf/ctags.vim
source $XDG_CONFIG_HOME/nvim/conf/doge.vim
source $XDG_CONFIG_HOME/nvim/conf/echodoc.vim
source $XDG_CONFIG_HOME/nvim/conf/editorconfig.vim
source $XDG_CONFIG_HOME/nvim/conf/elm.vim
source $XDG_CONFIG_HOME/nvim/conf/fzf.vim
source $XDG_CONFIG_HOME/nvim/conf/gutentags.vim
source $XDG_CONFIG_HOME/nvim/conf/lion.vim
source $XDG_CONFIG_HOME/nvim/conf/neomake.vim
source $XDG_CONFIG_HOME/nvim/conf/neoterm.vim
source $XDG_CONFIG_HOME/nvim/conf/netrw.vim
source $XDG_CONFIG_HOME/nvim/conf/rainbow_parentheses.vim
" source $XDG_CONFIG_HOME/nvim/conf/ranger.vim
source $XDG_CONFIG_HOME/nvim/conf/lf.vim
source $XDG_CONFIG_HOME/nvim/conf/signify.vim
source $XDG_CONFIG_HOME/nvim/conf/table-mode.vim
source $XDG_CONFIG_HOME/nvim/conf/tmux.vim
source $XDG_CONFIG_HOME/nvim/conf/ultisnips.vim
source $XDG_CONFIG_HOME/nvim/conf/undotree.vim
source $XDG_CONFIG_HOME/nvim/conf/vim-go.vim
source $XDG_CONFIG_HOME/nvim/conf/vim-test.vim
source $XDG_CONFIG_HOME/nvim/conf/vimux.vim
source $XDG_CONFIG_HOME/nvim/conf/vimwiki.vim
source $XDG_CONFIG_HOME/nvim/conf/vue.vim

" these need to be last to override defaults:
source $XDG_CONFIG_HOME/nvim/conf/settings.vim
source $XDG_CONFIG_HOME/nvim/conf/mappings.vim

" local vimrc overrides global settings
source $XDG_CONFIG_HOME/nvim/conf/local_vimrc.vim
